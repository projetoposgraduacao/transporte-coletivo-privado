'use strict'

// Module dependencies.
let express = require('express')
  , routes  = require('./routes')
  , vehicle = require('./routes/vehicle')
  , company = require('./routes/company');

let app = module.exports = express.createServer();

// Configuration
app.configure(() => {
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(app.router);
  app.use(express.static(__dirname + '/public'));
});

app.configure('development', () => {
  app.use(express.errorHandler({ dumpExceptions: true, showStack: true }));
});

app.configure('production', () => {
  app.use(express.errorHandler());
});

// Routes
app.get('/', routes.index);

// Vehicle API
app.get('/api/vehicle', vehicle.list);
app.get('/api/vehicle/:id', vehicle.get);
app.post('/api/vehicle', vehicle.create);
app.put('/api/vehicle/:id', vehicle.update);
app.delete('/api/vehicle/:id', vehicle.delete);

// Company API
app.get('/api/company', company.list);
app.get('/api/company/:id', company.get);
app.post('/api/company', company.create);
app.put('/api/company/:id', company.update);
app.delete('/api/company/:id', company.delete);

app.listen(3000, () => {
  console.log("Express server listening on port %d in %s mode", app.address().port, app.settings.env);
});