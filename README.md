# Transporte Coletivo Privado
Aplicativo para ajudar pessoas a encontrar a melhor opção de transporte coletivo privado

## Requisitos 

-   NodeJS
-   MongoDB

## API

-   Veículos

    -   [GET]  /api/vehicle
    -   [GET]  /api/vehicle/:id
    -   [PUT]  /api/vehicle/:id
    -   [POST] /api/vehicle

-   Empresas

    -   [GET]  /api/company
    -   [GET]  /api/company/:id
    -   [PUT]  /api/company/:id
    -   [POST] /api/company