'use strict'

let CompanyList   = require('../models/company/list')
  , CompanyGet    = require('../models/company/get')
  , CompanyCreate = require('../models/company/create')
  , CompanyUpdate = require('../models/company/update')
  , CompanyDelete = require('../models/company/delete');

exports.list   = CompanyList.list;
exports.get    = CompanyGet.get;
exports.create = CompanyCreate.create;
exports.update = CompanyUpdate.update;
exports.delete = CompanyDelete.delete;