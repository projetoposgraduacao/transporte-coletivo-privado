'use strict'

let VehicleList   = require('../models/vehicle/list')
  , VehicleGet    = require('../models/vehicle/get')
  , VehicleCreate = require('../models/vehicle/create')
  , VehicleUpdate = require('../models/vehicle/update')
  , VehicleDelete = require('../models/vehicle/delete');

exports.list   = VehicleList.list;
exports.get    = VehicleGet.get;
exports.create = VehicleCreate.create;
exports.update = VehicleUpdate.update;
exports.delete = VehicleDelete.delete;