'use strict'

let connection = require("../mongoose");

let mongoose = connection.mongoose
  , Schema = mongoose.Schema;

let Company = new Schema({
    name       : { type: String, default: '' }
  , description: { type: String, default: '' }
  , email      : { type: String, default: '' }
  , phone      : { type: String, default: '' }
  , address    : { type: String, default: '' }
  , city       : { type: String, default: '' }
  , state      : { type: String, default: '' }
  , long       : { type: Number, default: '' }
  , lat        : { type: Number, default: '' }
});

let CompanyModel = mongoose.model("Company", Company);

exports.company = CompanyModel;