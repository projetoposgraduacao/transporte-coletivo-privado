'use strict'

let model = require('./company')
  , Company = model.company;

exports.create = (req, res) => {
  let params = req.body
    , companyData = {
        name       : params.name
      , description: params.description
      , email      : params.email
      , phone      : params.phone
      , address    : params.address
      , city       : params.city
      , state      : params.state
      , long       : params.long
      , lat        : params.lat
      };

  let company = new Company(companyData);

  company.save(createCompany);

  function createCompany(err, data) {
    if (err) {
      res.json(err);
      return ;
    }

    res.json(data);
  }
};