'use strict'

let model = require('./company')
  , Company = model.company;

exports.list = (req, res) => {
  Company.find().exec(executeList);

  function executeList(err, companies) {
    if (err) {
      console.log(err);
      res.json(err);
      return ;
    }

    res.json(companies);
  };
};
