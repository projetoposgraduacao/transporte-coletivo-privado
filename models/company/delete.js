'use strict'

let model = require('./company')
  , Company = model.company;
  
exports.delete = (req, res) => {
  var query = { _id: req.params.id };

  Company.remove(query, executeDelete);

  function executeDelete(err, data) {
    if (err) {
      res.json(err);
      return ;
    }

    res.json(data);
  }
};