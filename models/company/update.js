'use strict'

let model = require('./company')
  , Company = model.company;

exports.update = (req, res) => {
  let query = { _id: req.params.id }
    , data = req.body;

  Company.update(query, data, executeUpdate);

  function executeUpdate(err, data) {
    if (err) {
      res.json(err);
      return ;
    }
    
    res.json(data);
  }
};