'use strict'

let model = require('./company')
  , Company = model.company;

exports.get = (req, res) => {
  let query = { _id: req.params.id };

  Company.findOne(query).exec(executeGet);

  function executeGet(err, user) {
    if (err) {
      res.json(err);
      return ;
    }
    
    res.json(user);
  };
};