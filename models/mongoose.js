'use strict'

let mongoose = require('mongoose');

mongoose.connect("mongodb://localhost/transporte");

let db = mongoose.connection;

db.on("error", (err) => {
  console.log("Error to connect: ", err);
});

db.once("open", () => {
  console.log("Connection successfull");
});

exports.mongoose = mongoose;