'use strict'

let model = require('./vehicle')
  , Vehicle = model.vehicle;
  
exports.delete = (req, res) => {
  var query = { _id: req.params.id };

  Vehicle.remove(query, executeDelete);

  function executeDelete(err, data) {
    if (err) {
      res.json(err);
      return ;
    }

    res.json(data);
  }
};