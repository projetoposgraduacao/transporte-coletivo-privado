'use strict'

let connection = require("../mongoose");

let mongoose = connection.mongoose
  , Schema = mongoose.Schema;

let Vehicle = new Schema({
    name       : { type: String, default: '' }
  , year       : { type: Number, default: '' }
  , type       : { type: String, default: '' }
  , capacityMax: { type: Number, default: '' }
  , observation: { type: String, default: '' }
  , company    : { type: String, default: '' }
});

let VehicleModel = mongoose.model("Vehicle", Vehicle);

exports.vehicle = VehicleModel;