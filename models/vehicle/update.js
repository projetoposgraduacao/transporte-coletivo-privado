'use strict'

let model = require('./vehicle')
  , Vehicle = model.vehicle;

exports.update = (req, res) => {
  let query = { _id: req.params.id }
    , data = req.body;

  Vehicle.update(query, data, executeUpdate);

  function executeUpdate(err, data) {
    if (err) {
      res.json(err);
      return ;
    }
    
    res.json(data);
  }
};