'use strict'

let model = require('./vehicle')
  , Vehicle = model.vehicle;

exports.get = (req, res) => {
  let query = { _id: req.params.id };

  Vehicle.findOne(query).exec(executeGet);

  function executeGet(err, user) {
    if (err) {
      res.json(err);
      return ;
    }
    
    res.json(user);
  };
};