'use strict'

let model = require('./vehicle')
  , Vehicle = model.vehicle;

exports.create = (req, res) => {
  let params = req.body
    , vehicleData = {
        name       : params.name
      , year       : params.year
      , type       : params.type
      , capacityMax: params.capacityMax
      , observation: params.observation
      , company    : params.company
      };

  let vehicle = new Vehicle(vehicleData);

  vehicle.save(createVehicle);

  function createVehicle(err, data) {
    if (err) {
      res.json(err);
      return ;
    }

    res.json(data);
  }
};