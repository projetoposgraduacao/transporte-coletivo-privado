'use strict'

let model = require('./vehicle')
  , Vehicle = model.vehicle;

exports.list = (req, res) => {
  Vehicle.find().exec(executeList);

  function executeList(err, vehicles) {
    if (err) {
      console.log(err);
      res.json(err);
      return ;
    }

    res.json(vehicles);
  };
};
